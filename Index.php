<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>	</title>
	<script type="text/javascript" src="lib/js/jquery-3.4.1.min.js"></script>
	
	<script>	
		$(document).ready(function(){

			$(".ingresar").click(function(e){
				e.preventDefault();
				$.ajax({
					url:"Diseño/form_ingreso.php",
					dataType:"html",
					success:function(data){
						$(".centro").html(data);
					}
				});
			});

			$(".consultar").click(function(e){
				e.preventDefault();
				$.ajax({
					url:"Diseño/Mostrar.php",
					dataType:"html",
					metod:"post",
					success:function(data){
						$(".centro").html(data);
					}
				});
			});

			
		});
	</script>
	<style>	
		body{
			background: gray;
		}
		.arriba{
			width: 80%;
			margin-left: 10%;
			height: 8vw;
			border:white solid 1px;
		}
		.arriba button{
			width: 20%;
			height: 3vw;
			margin-left: 20%;
			margin-top: 2vw;
			color: white;
			font-size: 1.5vw;
			background-color: green;
		}
		button:hover{
			cursor: pointer;
		}
		.centro{
			width: 80%;
			margin-left: 10%;
			border:white solid 1px;
			margin-top: 5vw;
		}
		table{
			width: 80%;
			margin-left: 10%;
			text-align: center;
		}
		table th{
			background: black;
			color: white;
			border-collapse: collapse;
		}
		#form_ing{
			width: 50%;
			margin-left: 25%;
			text-align: center;
		}
		input, select{
			border-radius: 25px;
			height: 2vw;
			font-size: 1.2vw;
			text-align: center;
		}
		input[type="submit"]{
			background: #077ee3;
			color: white;
			width: 15%;
			margin-bottom: 5vw;
		}
	</style>
</head>
<body> 
	<div class="arriba">
		<button class="ingresar">Ingresar</button><button class="consultar">Consultar</button>
	</div>
	<div class="centro">
		
	</div>
</body>
</html>